import json
import re

# import keras.preprocessing.text as T
import numpy as np
from keras.layers import LSTM, Dense, Input
from keras.models import Model

# from keras.preprocessing.text import Tokenizer
import tensorflow as tf
import sys
import random

batch_size = 64
epochs = 100
latent_dim = 256
path = "../MLDS_hw2_1_data/"

def progress(count, total, status=""):
    """ progress bar, just for fun """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s/%s ...%s\r" % (bar, count, total, status))
    sys.stdout.flush()


""" load training data """
with open(path + "training_label.json") as file:
    labels = json.load(file)
    label_dict = {}
    for i in labels:
        label_dict[i["id"]] = i["caption"]
# encoder_input_data, target = np.array([]), np.array([])
encoder_input_data,target=[],[]
for k,id in enumerate(label_dict.keys()):
    feat = np.load(path + "training_data/feat/" + id + ".npy")
    progress(k+1,1450,' loading data ')
    # trainFeats.append(np.load(path + "training_data/feat/" + id + ".npy"))
    for i in label_dict[id]:
        target.append(i)
        encoder_input_data.append(feat)

with open('./encoder_input_data.npy','w') as file:
    np.save(file,encoder_input_data)
    
with open('./target.npy','w') as file:
    np.save(file,target)
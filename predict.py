import numpy as np
from keras.layers import LSTM, Dense, Input
from keras.models import Model
from keras.models import load_model
# from keras.utils import plot_model

import sys
import json
path = "../MLDS_hw2_1_data/"


def progress(count, total, status=""):
    """ progress bar, just for fun """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s/%s ...%s\r" % (bar, count, total, status))
    sys.stdout.flush()


model = load_model('./seq2seq.h5')
# plot_model(model, to_file='model.png')
print(model.summary())
token_num, max_len = 0, 0
""" load parameters """
with open('parameters.json', 'r') as file:
    labels = json.load(file)
    k = labels['input2_shape']
    max_len = k[1]
    token_num = k[2]

""" load training data """
with open(path + "training_label.json") as file:
    labels = json.load(file)
    label_dict = {}
    for i in labels:
        label_dict[i["id"]] = i["caption"]

data = []
for k, id in enumerate(label_dict.keys()):
    feat = np.load(path + "training_data/feat/" + id + ".npy")
    progress(k + 1, len(label_dict), " loading data ")
    # trainFeats.append(np.load(path + "training_data/feat/" + id + ".npy"))
#     encoder_input_data.append(feat)
#     target.append(label_dict[id][0])
    data.append(feat)

data = np.array(data)
# for i in data:
with open('idx2word.json', 'r') as file:
    idx2word = json.load(file)
# print(idx2word)


def decode2word(a):
    try:
        # b = idx2word[a]
        b = idx2word[str(np.argmax(a))]
        if b != '<PAD>':
            print(b, end=' ')
    except:
        # b = '<UWK>'
        print('<UNK>', end=' ')


unused_input = np.zeros((data.shape[0], max_len, token_num))
print()
#     print(model.predict([i,unused_input]))
for i in model.predict([data, unused_input]):
    # print(i)
    for k in i:
        # print(decode2word(str(np.argmax(k))), end=' ')
        decode2word(k)
    print()
